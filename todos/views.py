from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.http import HttpRequest, HttpResponse
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def show_all_todo(request: HttpRequest) -> HttpResponse:
    todo_list_list = TodoList.objects.all()
    context = {"todo_list_all": todo_list_list}
    return render(request, "todos/list.html", context)


def show_todo(request: HttpRequest, id: int) -> HttpResponse:
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo_list_detail": todo}
    return render(request, "todos/todo.html", context)


def create_todo(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    elif request.method == "GET":
        form = TodoForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo(request: HttpRequest, id: int) -> HttpResponse:
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=post)
    context = {"todo_list_detail": post, "todo_form": form}
    return render(request, "todos/edit.html", context)


def delete_todo(request: HttpRequest, id: int) -> HttpResponse:
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.list.id)
    elif request.method == "GET":
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo_item(request: HttpRequest, id: int) -> HttpResponse:
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=post.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {"todo_list_detail": post, "todo_item_form": form}
    return render(request, "todos/update.html", context)
